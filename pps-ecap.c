/*
 * ECAP PPS driver
 *
 * Copyright (C) 2017 Kalle Koskinen <kvk@iki.fi>
 *
 * based on
 *
 * pps-gmtimer.c -- PPS client driver using OMAP Timers
 * Copyright (C) 2014  Daniel Drown <dan-android@drown.org>
 *
 * ECAP IIO pulse capture driver
 * Copyright (C) 2014 Linaro Limited
 * Author: Matt Porter <mporter@linaro.org>
 *
 * ECAP PWM driver
 * Copyright (C) 2012 Texas Instruments, Inc. - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/io.h>
#include <linux/err.h>
#include <linux/clk.h>
#include <linux/pm_runtime.h>
#include <linux/clocksource.h>
#include <linux/of_device.h>
#include <linux/interrupt.h>
#include <linux/pps_kernel.h>
#include <linux/timekeeping.h>
#include <linux/ktime.h>

/* ECAP register definitions  */
/* Timestamp counter register */
#define TSCTR			0x00
/* Timestamp capture 1-4 registers */
#define CAP1			0x08
#define CAP2			0x0C
#define CAP3			0x10
#define CAP4			0x14
/* Event capture control 1 register */
#define ECCTL1			0x28
#define CAPLDEN			BIT(8)
#define CAP_LOAD_ENABLE		BIT(8)
#define CAP4POL			BIT(6)
#define CAP4POL_FALLING		BIT(6)
#define CAP3POL			BIT(4)
#define CAP3POL_FALLING		BIT(4)
#define CAP2POL			BIT(2)
#define CAP2POL_FALLING		BIT(2)
#define CAP1POL			BIT(0)
#define CAP1POL_FALLING		BIT(0)
/* Event capture control 2 register */
#define ECCTL2			0x2A
#define TSCTRSTOP		BIT(4)
#define TSCTR_FREE_RUN		BIT(4)
/* Event capture interrupt register */
#define ECEINT			0x2C
#define CEVT4			BIT(4)
#define CAP4_EVENT_INT_ENABLE	BIT(4)
#define CEVT3			BIT(3)
#define CAP3_EVENT_INT_ENABLE	BIT(3)
#define CEVT2			BIT(2)
#define CAP2_EVENT_INT_ENABLE	BIT(2)
#define CEVT1			BIT(1)
#define CAP1_EVENT_INT_ENABLE	BIT(1)
/* Event capture flag register */
#define ECFLG			0x2E
/* Event capture clear register */
#define ECCLR			0x30

#define CAP_EVENT_MASK		(CEVT4|CEVT3|CEVT2|CEVT1)
#define CAP_EVENT_ALL		(CEVT4|CEVT3|CEVT2|CEVT1)

struct pps_ecap_drvdata {
	void __iomem *mmio_base;
	const char *timer_name;
	uint32_t frequency;
	uint32_t period;
	struct clocksource clksrc;
	int irq;
	struct pps_device *pps;
	struct pps_source_info info;
	u32 ctr_at_cap;
	u32 ctr_at_int;
	uint32_t latency_cycles;
	struct pps_event_time pps_ts;
	struct pps_event_time clear_ts;
	struct pps_event_time assert_ts;
};

static inline u32 ecap_read32(void __iomem *base, int offset)
{
	return readl(base + offset);
}

static inline u16 ecap_read(void __iomem *base, int offset)
{
	return readw(base + offset);
}

static inline void ecap_write(void __iomem *base, int offset, unsigned int val)
{
	writew(val & 0xFFFF, base + offset);
}

static void ecap_modify(void __iomem *base, int offset,
		unsigned short mask, unsigned short val)
{
	unsigned short regval;

	regval = readw(base + offset);
	regval &= ~mask;
	regval |= val & mask;
	writew(regval, base + offset);
}

static ssize_t ctr_at_cap_show(struct device *dev, struct device_attribute *attr, char *buf) {
	struct pps_ecap_drvdata *drvdata = dev_get_drvdata(dev);
	return sprintf(buf, "%lu\n", (unsigned long)drvdata->ctr_at_cap);
}
static DEVICE_ATTR(ctr_at_cap, S_IRUGO, ctr_at_cap_show, NULL);

static ssize_t ctr_at_int_show(struct device *dev, struct device_attribute *attr, char *buf) {
	struct pps_ecap_drvdata *drvdata = dev_get_drvdata(dev);
	return sprintf(buf, "%lu\n", (unsigned long)drvdata->ctr_at_int);
}
static DEVICE_ATTR(ctr_at_int, S_IRUGO, ctr_at_int_show, NULL);

static ssize_t interrupt_delta_show(struct device *dev, struct device_attribute *attr, char *buf) {
	struct pps_ecap_drvdata *drvdata = dev_get_drvdata(dev);
	return sprintf(buf, "%lu\n", (unsigned long)drvdata->latency_cycles);
}
static DEVICE_ATTR(interrupt_delta, S_IRUGO, interrupt_delta_show, NULL);

static ssize_t interrupt_timestamp_show(struct device *dev, struct device_attribute *attr, char *buf) {
	struct pps_ecap_drvdata *drvdata = dev_get_drvdata(dev);
	return sprintf(buf,"%lld.%09ld\n", (long long)drvdata->pps_ts.ts_real.tv_sec, drvdata->pps_ts.ts_real.tv_nsec );
}
static DEVICE_ATTR(interrupt_timestamp, S_IRUGO, interrupt_timestamp_show, NULL);

static ssize_t assert_timestamp_show(struct device *dev, struct device_attribute *attr, char *buf) {
	struct pps_ecap_drvdata *drvdata = dev_get_drvdata(dev);
	return sprintf(buf,"%lld.%09ld\n", (long long)drvdata->assert_ts.ts_real.tv_sec, drvdata->assert_ts.ts_real.tv_nsec );
}
static DEVICE_ATTR(assert_timestamp, S_IRUGO, assert_timestamp_show, NULL);

static ssize_t clear_timestamp_show(struct device *dev, struct device_attribute *attr, char *buf) {
	struct pps_ecap_drvdata *drvdata = dev_get_drvdata(dev);
	return sprintf(buf,"%lld.%09ld\n", (long long)drvdata->clear_ts.ts_real.tv_sec, drvdata->clear_ts.ts_real.tv_nsec );
}
static DEVICE_ATTR(clear_timestamp, S_IRUGO, clear_timestamp_show, NULL);

static struct attribute *attrs[] = {
	&dev_attr_ctr_at_cap.attr,
	&dev_attr_ctr_at_int.attr,
	&dev_attr_interrupt_delta.attr,
	&dev_attr_interrupt_timestamp.attr,
	&dev_attr_clear_timestamp.attr,
	&dev_attr_assert_timestamp.attr,
	NULL,
};
static struct attribute_group attr_group = {
   .attrs = attrs,
};

static const struct of_device_id pps_tiecap_dt_ids[] = {
	{ .compatible	= "acs,pps-ecap" },
	{ /* SENTINEL */},
};
MODULE_DEVICE_TABLE(of, pps_tiecap_dt_ids);

static irqreturn_t pps_ecap_interrupt(int irq, void *data)
{
	struct pps_ecap_drvdata *drvdata;
	struct system_time_snapshot snap;
	u16 irq_status;
	struct timespec64 latency;
	int event;

	drvdata = data;

	ktime_get_snapshot(&snap);

	irq_status = ecap_read(drvdata->mmio_base, ECFLG);

	switch (irq_status & CAP_EVENT_MASK) {
		case (CEVT1):
			drvdata->ctr_at_cap = ecap_read32(drvdata->mmio_base, CAP1);
			event = PPS_CAPTUREASSERT;
			drvdata->assert_ts.ts_real = ktime_to_timespec64(snap.real);
			ecap_write(drvdata->mmio_base, ECCLR, CEVT1);
			break;
		case (CEVT2):
			drvdata->ctr_at_cap = ecap_read32(drvdata->mmio_base, CAP2);
			event = PPS_CAPTURECLEAR;
			drvdata->clear_ts.ts_real = ktime_to_timespec64(snap.real);
			ecap_write(drvdata->mmio_base, ECCLR, CEVT2);
			break;
		case (CEVT3):
			drvdata->ctr_at_cap = ecap_read32(drvdata->mmio_base, CAP3);
			event = PPS_CAPTUREASSERT;
			drvdata->assert_ts.ts_real = ktime_to_timespec64(snap.real);
			ecap_write(drvdata->mmio_base, ECCLR, CEVT3);
			break;
		case (CEVT4):
			drvdata->ctr_at_cap = ecap_read32(drvdata->mmio_base, CAP4);
			event = PPS_CAPTURECLEAR;
			drvdata->clear_ts.ts_real = ktime_to_timespec64(snap.real);
			ecap_write(drvdata->mmio_base, ECCLR, CEVT4);
			break;
		default:
			pr_info("Bogus interrupt, all CEVT clear. ECFLG: %x\n", irq_status);
			return IRQ_HANDLED;
	}

	drvdata->ctr_at_int = snap.cycles;

	if (drvdata->ctr_at_int > drvdata->ctr_at_cap) {
		drvdata->latency_cycles = drvdata->ctr_at_int - drvdata->ctr_at_cap;
	} else {
		drvdata->latency_cycles = 4294967296
		                 - (drvdata->ctr_at_cap - drvdata->ctr_at_int) + 1;
	}

	latency.tv_sec = 0;
	latency.tv_nsec = drvdata->latency_cycles * drvdata->period;

	drvdata->pps_ts.ts_real = ktime_to_timespec64(snap.real);

	pps_sub_ts(&drvdata->pps_ts, latency);
	pps_event(drvdata->pps, &drvdata->pps_ts, event, NULL);

	return IRQ_HANDLED;
}

static struct pps_ecap_drvdata *of_get_pps_ecap_drvdata(struct platform_device *pdev)
{
	struct pps_ecap_drvdata *drvdata;
	struct resource *res;

	drvdata = devm_kzalloc(&pdev->dev,
                             sizeof(struct pps_ecap_drvdata),
                             GFP_KERNEL);
	if (!drvdata) {
		dev_err(&pdev->dev, "failed to allocate memory\n");
		goto fail;
	}

	/* We know the ECAPx clock rate, skip device tree */
	drvdata->frequency = 100000000;
	drvdata->period = 10;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	drvdata->mmio_base = devm_ioremap_resource(&pdev->dev, res);
	if (IS_ERR(drvdata->mmio_base)) {
		dev_err(&pdev->dev, "failed to get IORESOURCE_MEM\n");
		goto kfree_and_fail;
	}

	of_property_read_string_index(pdev->dev.of_node, "timer-name",
	                              0, &drvdata->timer_name);
	if (!drvdata->timer_name) {
		pr_err("timer-name property missing?\n");
		goto kfree_and_fail;
	}

	drvdata->irq = platform_get_irq(pdev, 0);
	if (drvdata->irq < 0) {
		dev_err(&pdev->dev, "failed to get interrupt number\n");
                goto kfree_and_fail;
	}

	return drvdata;

kfree_and_fail:
	devm_kfree(&pdev->dev, drvdata);
fail:
	return NULL;
}

static cycle_t pps_ecap_read_cycles(struct clocksource *cs)
{
	struct pps_ecap_drvdata *drvdata;

	drvdata = container_of(cs, struct pps_ecap_drvdata, clksrc);

	return (cycle_t)ecap_read32(drvdata->mmio_base, TSCTR);
}

static int ecap_clocksource_init(struct pps_ecap_drvdata *drvdata)
{
	int ret;

	ecap_modify(drvdata->mmio_base, ECCTL2, TSCTRSTOP, TSCTR_FREE_RUN);

	drvdata->clksrc.name = drvdata->timer_name;
	drvdata->clksrc.rating = 301;
	drvdata->clksrc.read = pps_ecap_read_cycles;
	drvdata->clksrc.mask = CLOCKSOURCE_MASK(32);
	drvdata->clksrc.flags = CLOCK_SOURCE_IS_CONTINUOUS;

	ret = clocksource_register_hz(&drvdata->clksrc, drvdata->frequency);
	if (!ret) {
		pr_info("clocksource: %s at %u Hz\n",
		        drvdata->clksrc.name, drvdata->frequency);
	} else {
		pr_err("Could not register clocksource %s\n",
		       drvdata->clksrc.name);
	}

	return ret;
}

static int ecap_pps_init(struct platform_device *pdev)
{
	struct pps_ecap_drvdata *drvdata;

	drvdata = platform_get_drvdata(pdev);

	drvdata->info.mode = PPS_CAPTUREBOTH | PPS_CANWAIT | PPS_TSFMT_TSPEC;
	drvdata->info.owner = THIS_MODULE;
	snprintf(drvdata->info.name, PPS_MAX_NAME_LEN - 1, "%s",
	         drvdata->timer_name);

	drvdata->pps = pps_register_source(&drvdata->info, PPS_CAPTUREBOTH);

	if (devm_request_irq(&pdev->dev, drvdata->irq,
	                     pps_ecap_interrupt, IRQF_TIMER,
	                     dev_name(&pdev->dev), drvdata)) {
		pr_err("cannot register IRQ %d\n", drvdata->irq);
		return -EIO;
	}

	ecap_modify(drvdata->mmio_base, ECCTL1, CAP2POL, CAP2POL_FALLING);
	ecap_modify(drvdata->mmio_base, ECCTL1, CAP4POL, CAP4POL_FALLING);
	ecap_modify(drvdata->mmio_base, ECCTL1, CAPLDEN, CAP_LOAD_ENABLE);
	ecap_modify(drvdata->mmio_base, ECEINT, CAP_EVENT_MASK, CAP_EVENT_ALL);

	return 0;
}

static int ecap_timer_probe(struct platform_device *pdev)
{
	struct pps_ecap_drvdata *drvdata;
	int ret;

	drvdata = of_get_pps_ecap_drvdata(pdev);

	platform_set_drvdata(pdev, drvdata);

	/* Enables the PWMSS clock */
	pm_runtime_enable(&pdev->dev);
	pm_runtime_get_sync(&pdev->dev);
	pm_runtime_forbid(&pdev->dev);

	ret = ecap_clocksource_init(drvdata);
	if (ret) {
		return ret;
	}

	ret = ecap_pps_init(pdev);
	if (ret) {
		return ret;
	}

	if(sysfs_create_group(&pdev->dev.kobj, &attr_group)) {
		pr_err("sysfs_create_group failed\n");
	}

	return 0;
}

static int ecap_timer_remove(struct platform_device *pdev)
{
	pm_runtime_allow(&pdev->dev);
	pm_runtime_put_sync(&pdev->dev);
	pm_runtime_disable(&pdev->dev);

	sysfs_remove_group(&pdev->dev.kobj, &attr_group);

	return 0;

}

static struct platform_driver pps_ecap_driver = {
	.driver = {
		.name	= "pps-ecap",
		.of_match_table = pps_tiecap_dt_ids,
	},
	.probe = ecap_timer_probe,
	.remove = ecap_timer_remove,
};

module_platform_driver(pps_ecap_driver);

MODULE_DESCRIPTION("ECAP capture driver");
MODULE_AUTHOR("Kalle Koskinen");
MODULE_LICENSE("GPL");
